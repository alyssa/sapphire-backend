/*
 * sapphire-backend
 *
 * Copyright (C) 2018 Alyssa Rosenzweig
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02111-1301  USA
 *
 */

#include <stdio.h>
#include <stdint.h>
#include <assert.h>

#include <glib.h>
#include <libsoup/soup.h>

#include <signal.h>
#include <string.h>
#ifndef _WIN32
#include <unistd.h>
#else
#include "win32/win32dep.h"
#endif
#include <fcntl.h>
#include <gio/gio.h>
#include <gio/gunixsocketaddress.h>

#include "json_compat.h"

/* Proxy configuration */
char *proxy_hostname = NULL;

typedef struct InstanceConnection {
	/* The reference counted proxied connection */
	GSocketConnection *connection;
	GDataInputStream *distream;

	/* The connections */
	GList *authenticated_connections;

	/* Who we are */
	struct SapphireAccount *account;
} InstanceConnection;

static void
sapphire_send_instance(struct InstanceConnection *proxy, const gchar *frame)
{
	if (!proxy->connection) {
		fprintf(stderr, "No proxy connection trying to send %s\n", frame);
		return;
	}

	GError *gerror;
	GOutputStream *ostream = g_io_stream_get_output_stream (G_IO_STREAM (proxy->connection));

	g_output_stream_write_all(ostream, frame, strlen(frame), NULL, NULL, &gerror);
	char end = '\n';
	g_output_stream_write(ostream, &end, 1, NULL, &gerror);
}

/* Represents a particular account */

typedef struct SapphireAccount {
	gchar *name;
	gchar *password_hash;

	/* Email if it is set, or NULL/empty string it not */
	gchar *push_notification_email;

	/* Instance connection associated with this account */
	InstanceConnection *instance;
} SapphireAccount;

/* Represents a connection to a given client. Minimal state should be kept
 * here, since connections are device-specific, not for the client as a whole.
 * Essentially, just enough for the websocket metadata and a little potpourrie */

typedef struct Connection {
	/* The reference counted connection itself */
	SoupWebsocketConnection *connection;

	/* The instance we're connected to */
	struct InstanceConnection *proxy;

	/* Has this connection authenticated yet? */
	gboolean is_authenticated;

	/* Remote IP Address. Must be g_free'd on destroy */
	gchar *ip_address;

	/* Associated attachment token */
	const gchar *token;
} Connection;

static void
sapphire_close_connection_instance(InstanceConnection *conn);

/* Whether this proxy supports multi-user mode */

static gboolean multi_user = FALSE;

/* Helper to serialize and broadcast */

#define WS_PORT 7070

static void
sapphire_send_raw_packet(InstanceConnection *instance, const char *packet)
{
	/* Broadcast to everyone interested in an instance */

	for (GList *it = instance->authenticated_connections; it != NULL; it = it->next) {
		Connection *conn = (Connection *) it->data;

		if (soup_websocket_connection_get_state(conn->connection) != SOUP_WEBSOCKET_STATE_OPEN) {
			/* TODO: Do we need to do anything here? */
			continue;
		}

		soup_websocket_connection_send_text(conn->connection, packet);
	}
}

/* Send to a specific connection */

static void
sapphire_send(Connection *conn, JsonObject *msg)
{
	if (soup_websocket_connection_get_state(conn->connection) != SOUP_WEBSOCKET_STATE_OPEN) {
		fprintf(stderr, "send: Attempt to send to a specific closed websocket\n");
		return;
	}

	gchar *str = json_object_to_string(msg);
	soup_websocket_connection_send_text(conn->connection, str);
	g_free(str);
}

static JsonNode *
json_parse_to_root(gchar *frame)
{
	JsonParser *parser = json_parser_new();

	if (!json_parser_load_from_data(parser, frame, -1, NULL)) {
		fprintf(stderr, "Error parsing response: %s\n", frame);
		return NULL;
	}

	return json_parser_get_root(parser);
}

/* Serialize accounts to/from disk. TODO: Serializing back */

GHashTable *username_to_account;

static void
sapphire_connect_instance(const gchar *name);

static void
sapphire_load_accounts(const gchar *name)
{
	GError *error = NULL;
	gchar *contents = NULL;
	gboolean success = g_file_get_contents(name, &contents, NULL, &error); 

	if (!success) {
		fprintf(stderr, "Failed to read accounts database\n");
		exit(1);
	}

	JsonNode *root = json_parse_to_root(contents);
	g_free(contents);

	if (root == NULL) {
		fprintf(stderr, "accounts: NULL root, ignoring\n");
		return;
	}

	JsonObject *obj = json_node_get_object(root);

	/* Some proxy info is stuffed in here as well */
	const char *hostname = json_object_get_string_member(obj, "hostname");

	if (hostname && strlen(hostname)) {
		proxy_hostname = g_strdup(hostname);
	}

	JsonArray *accounts = json_object_get_array_member(obj, "accounts");
	int len = json_array_get_length(accounts);

	if (len > 1 && !multi_user) {
		fprintf(stderr, "ERROR: Multi-user mode not enabled (did you enable TLS?) but multiple accounts in the database\n");
		exit(1);
	}

	for (int i = 0; i < len; ++i) {
		JsonObject *account = json_array_get_object_element(accounts, i);
		const gchar *name = json_object_get_string_member(account, "name");
		SapphireAccount *sapph = g_new0(SapphireAccount, 1);
		sapph->name = g_strdup(name);
		sapph->password_hash = g_strdup(json_object_get_string_member(account, "passwordHash"));
		sapph->push_notification_email = g_strdup(json_object_get_string_member(account, "pushNotificationEmail"));

		g_hash_table_insert(username_to_account, g_strdup(name), sapph);

		/* Try to connect the instance */
		sapphire_connect_instance(name);
	}
}

static void
sapphire_read_line(InstanceConnection *conn);

static void
sapphire_connect_instance(const gchar *name)
{
	/* Connect connect to the appropriate backend's socket */

	gchar *socket_path = multi_user ? g_strdup_printf("./accounts/%s/sockpuppet", name) : g_strdup("./sockpuppet");

	GSocketClient *client = g_socket_client_new();
	GSocketAddress *addr = g_unix_socket_address_new(socket_path);
	GSocketConnection *proxy_conn = g_socket_client_connect(client, G_SOCKET_CONNECTABLE(addr), NULL, NULL);

	if (!proxy_conn) {
		fprintf(stderr, "Failed to connect to proxy\n");

		return;
	}

	GInputStream *istream = g_io_stream_get_input_stream (G_IO_STREAM (proxy_conn));

	/* Create an instance object */
	InstanceConnection *instance = g_new0(InstanceConnection, 1);
	instance->connection = g_object_ref(proxy_conn);
	instance->distream = g_data_input_stream_new(istream);

	/* Start the async */
	sapphire_read_line(instance);

	/* Associate the instance object with the account */
	SapphireAccount *acct = (SapphireAccount *) g_hash_table_lookup(username_to_account, name); 
	acct->instance = instance;
	instance->account = (struct SapphireAccount *) acct;
}


/* Push notification routine (medium-specific). At the moment, push
 * notifications are email-based, allowing for SMS beeps via SMS-email bridge,
 * etc */

static void
sapphire_pushed(GObject *src, GAsyncResult *res, gpointer user_data)
{
	/* Push complete */

	gchar *recipient = (gchar *) user_data;
	printf("info: Pushed to %s\n", recipient);
	g_free(recipient);
}

static void
sapphire_push_notification_raw(InstanceConnection *conn, const gchar *content)
{
	/* Find the target mail address */
	const char *address = conn->account->push_notification_email;

	if (!address) {
		/* No address for push, so ignore */
		return;
	}

	/* Strip whitespace so it's zero length if blank */
	gchar *stripped = g_strstrip(g_strdup(address));

	if (stripped[0] == '\0') {
		fprintf(stderr, "Can't do push notifications to empty address, thonk\n");
		return;
	}

	/* Now, invoke mailx via system. XXX: SECURITY: Sanitize the address */
	GError *error = NULL;
	GSubprocess *subproc = g_subprocess_new(G_SUBPROCESS_FLAGS_STDIN_PIPE, &error, "mailx", "--", stripped, NULL);

	if (error) {
		fprintf(stderr, "push: Spawning error %s\n", error->message);
		return;
	}

	/* Write out the message, ending with an EOF */
	GBytes *content_bytes = g_bytes_new_static(content, strlen(content));
	g_subprocess_communicate(subproc, content_bytes, NULL, NULL, NULL, &error);

	if (error) {
		fprintf(stderr, "push: Sending error %s\n", error->message);
		return;
	}

	/* Let the process zombie for a moment to let the push go through, but
	 * don't block on it */

	g_subprocess_wait_async(subproc, NULL, sapphire_pushed, stripped);
}

/* Functions for icon proxying, TODO segregate by user */

GHashTable *username_to_icon;

/* Generic container for icons to paper over the difference between
 * PurpleStoredImage and PurpleBuddyIcon */

typedef struct {
	const gchar *extension;
	size_t size;
	gconstpointer data;
} SapphireIcon;

static void
soup_icon_callback(SoupServer *server,
                       SoupMessage *msg,
                       const char *path,
                       GHashTable *query,
                       SoupClientContext *client,
                       gpointer user_data)
{
	if (msg->method != SOUP_METHOD_GET) {
		soup_message_set_status (msg, SOUP_STATUS_NOT_IMPLEMENTED);
		return;
	}

	/* Extract the name from the query */

	if (!g_hash_table_contains(query, "name")) {
		soup_message_set_status (msg, SOUP_STATUS_NOT_FOUND);
		return;
	}

	const gchar *name = g_hash_table_lookup(query, "name");

	/* Search for icon */
	SapphireIcon *icon = g_hash_table_lookup(username_to_icon, name);

	if (!icon) {
		soup_message_set_status (msg, SOUP_STATUS_NOT_FOUND);
		return;
	}

	/* We found it, so return the icon appropriately */

	gchar *mimetype = g_strdup_printf("image/%s", icon->extension);

	if (!icon->data) {
		soup_message_set_status (msg, SOUP_STATUS_NOT_FOUND);
		return;
	}

	soup_message_set_status(msg, SOUP_STATUS_OK);
	soup_message_set_response(msg, mimetype, SOUP_MEMORY_TEMPORARY, icon->data, icon->size);

	/* Set cacheing header */
	soup_message_headers_append(msg->response_headers, "Cache-Control", "public, max-age=2592000");

	g_free(mimetype);
}

struct SoupServerMessage {
	SoupServer *server;
	SoupMessage *message;

	/* ID of the attachment in question (for uploads) */
	char *id;

	/* Mimetype and extension in question */
	char *mimetype;
	char *extension;

	GBytes *response;
};

/* Permitted mimetypes for attachments; dangerous file formats (including
 * archives) are not allowed. List culled from
 * https://developer.mozilla.org/en-US/docs/Web/HTTP/Basics_of_HTTP/MIME_types/Complete_list_of_MIME_types.
 */

static struct {
	const char *extension;
	const char *mimetype;
} allowed_mimetypes[] = {
	{"aac", "audio/aac"},
	{"bmp", "image/bmp"},
	{"css", "text/css"},
	{"csv", "text/csv"},
	{"epub", "application/epub+zip"},
	{"gif", "image/gif"},
	{"html", "text/html"},
	{"jpeg", "image/jpeg"},
	{"midi", "audio/midi"},
	{"midi", "audio/x-midi"},
	{"mpeg", "video/mpeg"},
	{"odp", "application/vnd.oasis.opendocument.presentation"},
	{"ods", "application/vnd.oasis.opendocument.spreadsheet"},
	{"odt", "application/vnd.oasis.opendocument.text"},
	{"ogg", "audio/ogg"},
	{"ogg", "video/ogg"},
	{"ogg", "application/ogg"},
	{"png", "image/png"},
	{"pdf", "application/pdf"},
	{"rtf", "application/rtf"},
	{"svg", "image/svg+xml"},
	{"tiff", "image/tiff"},
	{"txt", "text/plain"},
	{"wav", "audio/wav"},
	{"webm", "audio/webm"},
	{"webm", "video/webm"},
	{"webp", "image/webp"},
	{"zip", "application/zip"},
	{"7z", "application/x-7z-compress"},
	{"tar", "application/x-tar"},
	{"rar", "application/x-rar-compressed"},
	{"bin", "application/octet-stream"}
};

#ifndef ARRAY_SIZE
#define ARRAY_SIZE(x) (sizeof(x) / sizeof((x)[0]))
#endif

static const char *
sapphire_get_mimetype_from_message(SoupMessage *message)
{
	/* Make sure the mimetype is something we recognise */
	const gchar *mimetype = soup_message_headers_get_one(message->request_headers, "Content-Type");

	if (mimetype) {
		for (unsigned i = 0; i < ARRAY_SIZE(allowed_mimetypes); ++i) {
			if (g_strcmp0(mimetype, allowed_mimetypes[i].mimetype) == 0) {
				return allowed_mimetypes[i].mimetype;
			}
		}
	}

	/* Otherwise, default back on something sketchy */
	return "application/octet-stream";
}

static const char *
sapphire_extension_from_mimetype(const char *mimetype) {
	for (unsigned i = 0; i < ARRAY_SIZE(allowed_mimetypes); ++i) {
		if (g_strcmp0(mimetype, allowed_mimetypes[i].mimetype) == 0) {
			return allowed_mimetypes[i].extension;
		}
	}

	return NULL;
}

static const char *
sapphire_mimetype_from_extension(const char *extension) {
	for (unsigned i = 0; i < ARRAY_SIZE(allowed_mimetypes); ++i) {
		if (g_strcmp0(extension, allowed_mimetypes[i].extension) == 0) {
			return allowed_mimetypes[i].mimetype;
		}
	}

	return NULL;
}

static GFile *
sapphire_make_attachment_file(const char *id, const char *extension)
{
	/* Get rid of slash in mimetype which interferes with file separators */
	gchar *name = g_strdup_printf("%s.%s", id, extension);
	GFile *file = g_file_new_build_filename("attachments", name, NULL);
	g_free(name);

	return file;
}

/* Fetch an attachment from disk */

static void
sapphire_got_attachment(GObject *source_object,
		GAsyncResult *res,
		gpointer user_data);

static void
sapphire_fetch_attachment(struct SoupServerMessage *message, const char *path)
{
	const gchar *attachment_prefix = "/attachment/";
	size_t uuid_length = 36;

	if (strlen(path) < (strlen(attachment_prefix) + uuid_length + 1)) {
		/* Path too short -- don't want to read out-of-bounds! */
		soup_message_set_status (message->message, SOUP_STATUS_NOT_FOUND);
		return;
	}

	/* Parse out the parts of /attachment/uuuid.png based on lengths */
	const gchar *id = g_strndup(path + strlen(attachment_prefix), uuid_length);
	const gchar *extension = path + strlen(attachment_prefix) + uuid_length + 1;

	if (!id || !g_uuid_string_is_valid(id)) {
		soup_message_set_status (message->message, SOUP_STATUS_NOT_FOUND);
		return;
	}

	const char *mimetype = sapphire_mimetype_from_extension(extension);

	if (!mimetype) {
		soup_message_set_status (message->message, SOUP_STATUS_NOT_FOUND);
		return;
	}

	message->mimetype = g_strdup(mimetype);

	/* Try to read the attachment from disk */
	GFile *file = sapphire_make_attachment_file(id, extension);
	g_file_load_contents_async(file, NULL, sapphire_got_attachment, message);

	/* Pause while we wait on the disk */
	soup_server_pause_message(message->server, message->message);
}

static void
sapphire_got_attachment(GObject *source_object,
		GAsyncResult *res,
		gpointer user_data)
{
	GFile *file = G_FILE(source_object);
	struct SoupServerMessage *msg = (struct SoupServerMessage *) user_data;

	char *contents;
	gsize length;
	GError *error = NULL;
	g_file_load_contents_finish(file, res, &contents, &length, NULL, &error);

	/* Unpause the message now that we're ready */
	soup_server_unpause_message(msg->server, msg->message);

	if (error) {
		soup_message_set_status(msg->message, SOUP_STATUS_NOT_FOUND);
	} else {
		/* Serve the contents */
		soup_message_set_status(msg->message, SOUP_STATUS_OK);
		soup_message_set_response(msg->message, msg->mimetype, SOUP_MEMORY_TAKE, contents, length);
	}

	g_free(msg->mimetype);
}

/* Hashmap of valid attachment token to Connection (we own the memory for the
 * token but not the Connection). When a new connection is established, a token
 * is generated for it. When the connection is closed, the token is revoked.
 * The scheme isn't perfect, but for now it'll prevent most abuses of the
 * upload service from outsiders
 */

GHashTable *attachment_token_to_connection;

/* Generate a token and return it. The memory is consumed by the hash table,
 * hence why the caller doesn't g_free anything */

static void
sapphire_generate_token_connection(Connection *conn)
{
	conn->token = g_uuid_string_random();
	g_hash_table_insert(attachment_token_to_connection, g_strdup(conn->token), conn);

	JsonObject *msg = json_object_new();
	json_object_set_string_member(msg, "op", "token");
	json_object_set_string_member(msg, "token", conn->token);
	sapphire_send(conn, msg);
	json_object_unref(msg);
}

static void
sapphire_got_created(GObject *source_object,
		GAsyncResult *res,
		gpointer user_data);

static Connection *
sapphire_check_token(GHashTable *query) {
	const gchar *token = g_hash_table_lookup(query, "token");
	return g_hash_table_lookup(attachment_token_to_connection, token);

}

static void
sapphire_upload_attachment(struct SoupServerMessage *msg, GHashTable *query)
{
	/* Ensure that we're authorized */

	if (!sapphire_check_token(query)) {
		soup_message_set_status(msg->message, SOUP_STATUS_FORBIDDEN);
		return;
	}

	/* Generate a UUID for the attachment */
	const char *mimetype = sapphire_get_mimetype_from_message(msg->message);

	if (!mimetype) {
		/* Bad mimetype */
		soup_message_set_status(msg->message, SOUP_STATUS_FORBIDDEN);
		return;
	}

	msg->id = g_uuid_string_random();
	msg->mimetype = g_strdup(mimetype);
	msg->extension = g_strdup(sapphire_extension_from_mimetype(mimetype));

	/* Fetch the response */
	SoupBuffer *buffer = soup_message_body_flatten(msg->message->request_body);
	msg->response = soup_buffer_get_as_bytes(buffer);

	/* Create the file */
	GFile *file = sapphire_make_attachment_file(msg->id, msg->extension);

	g_file_create_readwrite_async(file, G_FILE_CREATE_NONE, G_PRIORITY_DEFAULT, NULL, sapphire_got_created, msg);

	/* Again, we're waiting on disk */
	soup_server_pause_message(msg->server, msg->message);
}

static void
sapphire_got_created(GObject *source_object,
		GAsyncResult *res,
		gpointer user_data)
{
	GFile *file = G_FILE(source_object);
	struct SoupServerMessage *msg = (struct SoupServerMessage *) user_data;

	soup_server_unpause_message(msg->server, msg->message);

	GError *error = NULL;
	GFileIOStream *io = g_file_create_readwrite_finish(file, res, &error);

	if (error) {
		fprintf(stderr, "Error creating attachment: %s\n", error->message);
		soup_message_set_status(msg->message, SOUP_STATUS_NOT_FOUND);
		return;
	}

	GOutputStream *os = g_io_stream_get_output_stream(G_IO_STREAM(io));

	size_t size = 0;
	const char *data = g_bytes_get_data(msg->response, &size);
	gboolean success = g_output_stream_write_all(os, data, size, NULL, NULL, &error);

	if (error || !success) {
		fprintf(stderr, "Error writing attachment\n");
		soup_message_set_status(msg->message, SOUP_STATUS_NOT_FOUND);
		return;
	}

	/* Unpause the message now that we're ready */
	soup_message_set_status(msg->message, SOUP_STATUS_OK);

	/* Tell them where to fetch the result from. TODO: Actually maybe just
	 * require the base to be deliberately configured */

	gchar *url = g_strdup_printf("http%s://%s:%d/attachment/%s.%s",
			multi_user ? "s" : "",
			proxy_hostname ? proxy_hostname : g_get_host_name(),
			WS_PORT,
			msg->id,
			msg->extension);

	JsonObject *resp = json_object_new();
	json_object_set_string_member(resp, "url", url);
	gchar *str = json_object_to_string(resp);
	soup_message_set_response(msg->message, "text/json", SOUP_MEMORY_TAKE, str, strlen(str));
	json_object_unref(resp);
	g_free(url);

	/* Clean up */
	g_free(msg->id);
	g_free(msg->mimetype);
	g_free(msg->extension);
}

static void
soup_attachment_callback(SoupServer *server,
                       SoupMessage *msg,
                       const char *path,
                       GHashTable *query,
                       SoupClientContext *client,
                       gpointer user_data)
{
	/* Delegate based on callback */

	struct SoupServerMessage smsg = {
		.server = server,
		.message = msg
	};

	struct SoupServerMessage *p_msg = g_memdup(&smsg, sizeof(smsg));

	/* Liberal CORS policy */
	soup_message_headers_append(msg->response_headers, "Access-Control-Allow-Origin", "*");
	soup_message_headers_append(msg->response_headers, "Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept, Range");

	if (msg->method == SOUP_METHOD_GET) {
		sapphire_fetch_attachment(p_msg, path);
	} else if (query && msg->method == SOUP_METHOD_POST) {
		sapphire_upload_attachment(p_msg, query);
	} else if (msg->method == SOUP_METHOD_OPTIONS) {
		soup_message_set_status (msg, SOUP_STATUS_OK);
		soup_message_headers_append(msg->response_headers, "Access-Control-Allow-Methods", "POST, GET");
	} else {
		soup_message_set_status (msg, SOUP_STATUS_NOT_IMPLEMENTED);
		g_free(p_msg);
	}
}

static void
soup_beacon_callback(SoupServer *server,
                       SoupMessage *msg,
                       const char *path,
                       GHashTable *query,
                       SoupClientContext *client,
                       gpointer user_data)
{
	soup_message_headers_append(msg->response_headers, "Access-Control-Allow-Origin", "*");
	soup_message_headers_append(msg->response_headers, "Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept, Range");

	if (msg->method == SOUP_METHOD_POST) {
		/* Figure out who's sending */
		Connection *conn = sapphire_check_token(query);

		if (!conn) {
			soup_message_set_status(msg, SOUP_STATUS_FORBIDDEN);
			return;
		}
		printf("Got a beacon\n");

		/* Grab the payload */
		SoupBuffer *buffer = soup_message_body_flatten(msg->request_body);
		GBytes *payload = soup_buffer_get_as_bytes(buffer);

		if (!payload) {
			soup_message_set_status(msg, SOUP_STATUS_NOT_FOUND);
			return;
		}

		gsize size;
		guint8 *payload8_buffer = g_bytes_unref_to_data(payload, &size);

		/* Null terminate */
		gchar *payload8 = g_strndup((gchar *) payload8_buffer, size);
		g_free(payload8_buffer);

		if (!payload8) {
			soup_message_set_status(msg, SOUP_STATUS_NOT_FOUND);
			return;
		}

		JsonNode *root = json_parse_to_root((gchar *) payload8);
		g_free(payload8);

		if (!root) {
			soup_message_set_status(msg, SOUP_STATUS_NOT_FOUND);
			return;
		}

		JsonObject *obj = json_node_get_object(root);

		if (!obj) {
			soup_message_set_status(msg, SOUP_STATUS_NOT_FOUND);
			return;
		}

		/* We have the data, iterate for each buddy */
		JsonObjectIter iter;
		const gchar *buddy_id;
		JsonNode *buddy_node;

		json_object_iter_init(&iter, obj);
		while (json_object_iter_next(&iter, &buddy_id, &buddy_node)) {
			const gchar *msg = json_node_get_string(buddy_node);

			if (!msg) {
				printf("Skipping unknown message for buddy %s\n", buddy_id);
				continue;
			}

			/* Construct a message op for us */

			JsonObject *data = json_object_new();
			json_object_set_string_member(data, "op", "message");
			json_object_set_string_member(data, "buddy", buddy_id);
			json_object_set_string_member(data, "content", msg);
			json_object_set_int_member(data, "flags", 1);

			/* Send off the message to the buddy */

			gchar *str = json_object_to_string(data);
			printf("%s\n", str);
			sapphire_send_instance(conn->proxy, str);
			g_free(str);
			json_object_unref(data);
		}

		soup_message_set_status (msg, SOUP_STATUS_OK);
	} else if (msg->method == SOUP_METHOD_OPTIONS) {
		soup_message_set_status (msg, SOUP_STATUS_OK);
		soup_message_headers_append(msg->response_headers, "Access-Control-Allow-Methods", "POST, GET");
	}
}


/* IP -> rate limit hash table. Rate limits are integers of the number of
 * seconds rate limited. If positive, the rate limit is active. If negative, it
 * is inactive and merely stored for posterity. Rate limits are mostly reset on
 * a successful authentication */

GHashTable *rate_limits;

/* Callback for a timer once the rate limit is restored for the next attempt */

static gboolean
sapphire_restore_rate_limit(gpointer user_data)
{
	gchar *ip_address = (gchar *) user_data;

	int limit = abs(GPOINTER_TO_INT(g_hash_table_lookup(rate_limits, ip_address)));
	g_hash_table_replace(rate_limits, ip_address, GINT_TO_POINTER(-limit));

	/* We can't free the ip address yet, since it still needs it for the
	 * key. TODO: How not to leak? */

	return FALSE;
}

static void
sapphire_got_internal(InstanceConnection *conn, gchar *frame)
{
	JsonNode *root = json_parse_to_root(frame);

	if (root == NULL) {
		fprintf(stderr, "internal: NULL root, ignoring\n");
		return;
	}

	JsonObject *obj = json_node_get_object(root); 

	const gchar *op = json_object_get_string_member(obj, "op");

	if (g_strcmp0(op, "icon") == 0) {
		SapphireIcon *icon = g_new0(SapphireIcon, 1);

		const gchar *name = json_object_get_string_member(obj, "name");
		const gchar *base64 = json_object_get_string_member(obj, "base64");

		icon->extension = json_object_get_string_member(obj, "ext");
		icon->data = g_base64_decode(base64, &icon->size);

		g_hash_table_insert(username_to_icon, g_strdup(name), icon);
	} else if (g_strcmp0(op, "push") == 0) {
		const gchar *content = json_object_get_string_member(obj, "content");
		sapphire_push_notification_raw(conn, content);
	} else {
		fprintf(stderr, "internal: Unknown op %s\n", op);
		return;
	}
}

static void sapphire_got_line(GObject *source_object, GAsyncResult *res, gpointer user_data);

static void
sapphire_read_line(InstanceConnection *conn)
{
	g_data_input_stream_read_line_async(conn->distream, G_PRIORITY_DEFAULT, NULL, sapphire_got_line, conn);
}

static void
sapphire_got_line(GObject *source_object,
                        GAsyncResult *res,
                        gpointer user_data)
{
	InstanceConnection *conn = (InstanceConnection *) user_data;

	GError *err = NULL;
	gsize len;
	char *data = g_data_input_stream_read_line_finish_utf8(G_DATA_INPUT_STREAM(source_object), res, &len, &err);

	if (err || !data) {
		/* Borp, error -- disconnect (and handle, somehow..) */
		fprintf(stderr, "Disconnecting internal socket: %s\n", err ? err->message : "");

		if (data)
			g_free(data);

		sapphire_close_connection_instance(conn);

		return;
	}

	/* Check the first character. If it's >, this is an internal packet. Otherwise, pass it along */

	if (data[0] == '>') {
		sapphire_got_internal(conn, data + 1);
	} else {
		sapphire_send_raw_packet(conn, data);
	}

	g_free(data);

	sapphire_read_line(conn);
}

static gboolean
sapphire_try_login(Connection *conn, const char *username, const char *attempted_hash);

static void
sapphire_send_rate_limit(Connection *conn, int ms);

static void
soup_ws_data(SoupWebsocketConnection *self,
               gint                     type,
               GBytes                  *message,
               gpointer                 user_data)
{
	struct Connection *conn = (struct Connection *) user_data;

	const gchar *frame = (const gchar *) g_bytes_get_data(message, NULL);

	/* The message should be interpreted as JSON, decode that here */

	JsonParser *parser = json_parser_new();

	if (!json_parser_load_from_data(parser, frame, -1, NULL)) {
		fprintf(stderr, "Error parsing response: ...\n");
		return;
	}

	JsonNode *root = json_parser_get_root(parser);

	if (root == NULL) {
		fprintf(stderr, "websocket: NULL root, ignoring\n");
		return;
	}

	/* How to proceed depends if we're authenticated or not. If we are,
	 * this is a standard client message, ready to be parsed, relayed, and
	 * actuated. If we are not, this is an authentication message (by
	 * definition -- otherwise they get booted to penalize credential
	 * attacks */

	if (conn->is_authenticated) {
		/* Forward the packet */
		sapphire_send_instance(conn->proxy, frame);
	} else {
		JsonObject *obj = json_node_get_object(root);

		gboolean success = FALSE;
		const char *username;

		if (obj) {
			username = json_object_get_string_member(obj, "username");

			const char *passwordHash = json_object_get_string_member(obj, "passwordHash");

			if (username && passwordHash) {
				success = sapphire_try_login(conn, username, passwordHash);
			}
		}

		if (!success) {
			/* Slow down future attempts for rate limiting */
			int limit = abs(GPOINTER_TO_INT(g_hash_table_lookup(rate_limits, conn->ip_address)));

			if (limit == 0) {
				/* Initial rate limit of 320ms, grows exponentially by two's */
				limit = 1;
			} 

			/* Exponential rate limit growth for repeat offenders */
			limit *= 2;

			/* Store that limit */
			g_hash_table_insert(rate_limits, g_strdup(conn->ip_address), GINT_TO_POINTER(limit));

			/* Create a timeout to restore their access */
			int milliseconds = limit * 160;
			g_timeout_add(milliseconds, sapphire_restore_rate_limit, g_strdup(conn->ip_address));

			/* Tell the client how long we're rate limiting them for */
			sapphire_send_rate_limit(conn, milliseconds);

			/* Eject */
			const char *error = "Authentication error";
			soup_websocket_connection_close(conn->connection, SOUP_WEBSOCKET_CLOSE_POLICY_VIOLATION, error);

			return;
		} else {
			/* Successful login - so get rid of the rate limit */

			g_hash_table_replace(rate_limits, g_strdup(conn->ip_address), GINT_TO_POINTER(0));
		}



		/* To authenticate, set the flag and add us to the list */
		conn->is_authenticated = TRUE;	

		/* Remove size limit -- needed for avatar upload, etc. TODO: Is this risky? */
		soup_websocket_connection_set_max_incoming_payload_size(conn->connection, 0);

		SapphireAccount *account = g_hash_table_lookup(username_to_account, username);

		if (!account) {
			fprintf(stderr, "Bad acccount on connection, somehow\n");
			return;
		}

		InstanceConnection *instance = account->instance;

		if (!instance) {
			fprintf(stderr, "Missing instance on connection?\n");
			return;
		}

		/* Append us to the instance list */
		instance->authenticated_connections = g_list_append(instance->authenticated_connections, conn);

		/* And associate in the other direction */
		conn->proxy = instance;

		/* Make sure there's actually an instance to talk to. If
		 * there's not, make a last-minute effort to try to connect to
		 * the instance again, "resurrecting" it after a failure. TODO:
		 * This is not particularly robust */

		if (!instance->connection) {
			sapphire_connect_instance(account->name);
		}

		/* Generate an attachment token */
		sapphire_generate_token_connection(conn);

		/* Ask for the world */
		sapphire_send_instance(conn->proxy, "{\"op\":\"world\"}");
	}

	g_object_unref(parser);
}

static void
soup_ws_error(SoupWebsocketConnection *self,
		GError *gerror,
               gpointer                 user_data)
{
	fprintf(stderr, "Websocket Error\n");
}

static void
sapphire_close_connection_instance(InstanceConnection *conn)
{
	/* Disconnect the proxy-half of the connection */
	if (conn->connection) {
		g_io_stream_close(G_IO_STREAM(conn->connection), NULL, NULL);
		conn->connection = NULL;
	}
}

static void
sapphire_close_connection(Connection *conn)
{
	if (!conn) {
		/* No conn to close */
		return;
	}

	/* Free connection */
	if (conn->ip_address) {
		g_free(conn->ip_address);
		conn->ip_address = NULL;
	}

	if (conn->proxy) {
		/* Splice the socket out of the authenticated list, so we no longer
		 * attempt to broadcast to it */

		conn->proxy->authenticated_connections = g_list_remove(conn->proxy->authenticated_connections, conn);

		if (conn->proxy->authenticated_connections == NULL) {
			/* Alert the backend we're gone */
			sapphire_send_instance(conn->proxy, "{\"op\":\"ghost\"}");
		}
	}

	if (conn->token) {
		/* Revoke the token */
		g_hash_table_remove(attachment_token_to_connection, conn->token);
		conn->token = NULL;
	}
}

static void
soup_ws_closed(SoupWebsocketConnection *self,
               gpointer                 user_data)
{
	Connection *conn = (Connection *) user_data;
	sapphire_close_connection(conn);
}

static void
soup_ws_callback(SoupServer *server,
			SoupWebsocketConnection *connection,
			const char *path,
			SoupClientContext *client,
			gpointer user_data)
{
	/* Figure out who we're talking to */
	GSocketAddress *socket_address = soup_client_context_get_remote_address(client);

	GSocketFamily family = g_socket_address_get_family(socket_address);

	if ((family != G_SOCKET_FAMILY_IPV4) && (family != G_SOCKET_FAMILY_IPV6)) {
		/* Should be unreachable */
		fprintf(stderr, "Non-IP socket?\n");
		return;
	}

	GInetAddress *inet_address = g_inet_socket_address_get_address((GInetSocketAddress *) socket_address);
	gchar *addr = g_inet_address_to_string(inet_address);

	/* Allocate a connection object for us and fill it in */
	Connection *conn = g_new0(Connection, 1);

	conn->is_authenticated = FALSE;

	/* Save the connection.
	 * IMPORTANT: Reference counting is necessary to keep the connection
	 * alive. No idea why this isn't documented anywhere, xxx
	 */

	conn->connection = g_object_ref(connection);

	/* Save the IP for ratelimiting */
	conn->ip_address = addr;

	/* Check for rate limiting, for that matter */
	int rate_limit = GPOINTER_TO_INT(g_hash_table_lookup(rate_limits, conn->ip_address));

	if (rate_limit > 0) {
		/* Violation -- eject */
		const char *error = "Rate limit violation";
		soup_websocket_connection_close(conn->connection, SOUP_WEBSOCKET_CLOSE_POLICY_VIOLATION, error);
		fprintf(stderr, "Ejecting rate limit violation from %s\n", conn->ip_address);
		return;
	}

	/* Subscribe to the various signals */

	g_signal_connect(connection, "message", G_CALLBACK(soup_ws_data), conn);
	g_signal_connect(connection, "closed", G_CALLBACK(soup_ws_closed), conn);
	g_signal_connect(connection, "error", G_CALLBACK(soup_ws_error), conn);
}

static void
sapphire_init_websocket(gboolean secure)
{
	GError *error = NULL;

	SoupServer *soup = soup_server_new(NULL, NULL);

	if (secure) {
		gchar *key_file = "key.pem";
		gchar *cert_file = "cert.pem";

		if (!soup_server_set_ssl_cert_file(soup, cert_file, key_file, &error)) {
			fprintf(stderr, "Error setting SSL certificate\n");
			fprintf(stderr, "Msg: %s\n", error->message);
			exit(1);
		}
	} else {
		/* Be a little scary */
		fprintf(stderr, "* * *\n\nWARNING: RUNNING IN PLAIN HTTP MODE!!! DO NOT DEPLOY IN PRODUCTION!!!\n\n* * *\n\n");
	}


	/* Initialize icon and attachment endpoints */
	soup_server_add_handler(soup, "/icon/", soup_icon_callback, NULL, NULL);
	soup_server_add_handler(soup, "/attachment/", soup_attachment_callback, NULL, NULL);
	soup_server_add_handler(soup, "/send/", soup_beacon_callback, NULL, NULL);

	/* WebSocket entrypoint */
	char *protocols[] = { "binary", NULL };
	soup_server_add_websocket_handler(soup, "/ws", NULL, protocols, soup_ws_callback, NULL, NULL);

	if (!soup_server_listen_all(soup, WS_PORT, secure ? SOUP_SERVER_LISTEN_HTTPS : 0, &error)) {
		fprintf(stderr, "Error listening in soup\n");
		fprintf(stderr, "Msg: %s\n", error->message);
		exit(1);
	}
}



#include "secure-compare-64.h"

static gboolean
sapphire_try_login(Connection *conn, const char *username, const char *attempted_hash)
{
	gboolean success = FALSE;

	/* Find the appropriate account */

	SapphireAccount *account = g_hash_table_lookup(username_to_account, username);

	if (!account) {
		fprintf(stderr, "login: Bad account\n");
		return FALSE;
	}

	/* Valid hashes must be 64 bytes (32 bytes of binary -> 64 of hex) */

	if (strlen(attempted_hash) == 64 && strlen(account->password_hash) == 64) {
		/* Fetch the SHA-256 secret */
		success = secure_compare_64(attempted_hash, account->password_hash);
	}

	/* TODO: Rate limit on failure */

	/* Packet indicating status */

	if (success) {
		JsonObject *data = json_object_new();
		json_object_set_string_member(data, "op", "authsuccess");
		sapphire_send(conn, data);
		json_object_unref(data);
	}

	return success;
}

static void
sapphire_send_rate_limit(Connection *conn, int ms)
{
	JsonObject *data = json_object_new();
	json_object_set_string_member(data, "op", "ratelimit");
	json_object_set_int_member(data, "milliseconds", ms);
	sapphire_send(conn, data);
	json_object_unref(data);
}

GList *authenticated_connections;

int main(int argc, char *argv[])
{
	GMainLoop *loop;

#ifdef _WIN32
	g_thread_init(NULL);
#endif

	g_set_prgname("Sapphire-Proxy");
	g_set_application_name("Sapphire-Proxy");

	loop = g_main_loop_new(NULL, FALSE);
	g_main_loop_ref(loop);

	/* Initialize icon database */
	username_to_icon = g_hash_table_new(g_str_hash, g_str_equal);
	rate_limits = g_hash_table_new(g_str_hash, g_str_equal);
	username_to_account = g_hash_table_new(g_str_hash, g_str_equal);
	attachment_token_to_connection = g_hash_table_new_full(g_str_hash, g_str_equal, g_free, NULL);

	gboolean secure = (argc >= 2) && (g_strcmp0(argv[1], "--production") == 0);

	/* Only allow multi-user mode if we're TLS-encrypted */
	multi_user = secure;

	const gchar *database_path = (argc >= 3) ? argv[2] : "./sapphire-accounts.json";
	sapphire_load_accounts(database_path);

	sapphire_init_websocket(secure);

	g_main_context_iteration(g_main_loop_get_context(loop), FALSE);

	g_main_loop_run(loop);

	return 0;
}
